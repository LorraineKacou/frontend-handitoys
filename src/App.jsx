
import './App.css'
import ToyList from './components/ToyList'

function App() {
  

  return (
    <div className="App">
    <ToyList/>
    </div>
  )
}

export default App
