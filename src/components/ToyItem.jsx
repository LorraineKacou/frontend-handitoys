import React from "react"

function ToyItem(props){

    const{id,image, description,title}=props;

    return(
        <li className="ToyItem">
        <p>id:{id}</p>
        <img src={image}  alt={title}/>  
        <p> {description}</p>
        <h3> {title} </h3>
        </li>
    )

}

export default ToyItem