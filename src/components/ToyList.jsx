import React, { useState } from "react"
import ToyItem from "./ToyItem"
import axios from "axios"
import { useEffect } from "react";

function ToyList(){

 const API_URL="http://localhost:8080/toys";


 const[toyList, setToyList]= useState([]);

 useEffect(()=>{
    axios.get(API_URL).then((response)=>{
        setToyList(response.data);
       });
 },[]);

 

    return(
        <div className="ToyList">
            <ul>
            {toyList.map((toy)=>(
            <ToyItem 
            key={toy.toyId}
            id={toy.toyId}
            image={toy.image}
            description={toy.description}
            title={toy.title}/>  
            ))}
            </ul>
            
        </div>
    )

}

export default ToyList
